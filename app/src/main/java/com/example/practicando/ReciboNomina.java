package com.example.practicando;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtra;
    private int puesto;
    private float impuestoPorc;

    //Constructor
    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtra,
                        int puesto, float impuestoPorc){
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtra = horasTrabExtra;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;

    }

    //Métodos Set y Get
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtra() {
        return horasTrabExtra;
    }

    public void setHorasTrabExtra(float horasTrabExtra) {
        this.horasTrabExtra = horasTrabExtra;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    //Calculos - funciones
    public float calcularSubtotal(int puesto, float horasTrabNormal, float horasTrabExtra){
        float subtotal;
        float pagoBase = 200;
        if (puesto == 1){
            pagoBase = (float) ((pagoBase * 0.20) + pagoBase);
        } else if (puesto == 2) {
            pagoBase = (float) ((pagoBase * 0.50) + pagoBase);
        } else if (puesto == 3) {
            pagoBase = (float) ((pagoBase * 1.0) + pagoBase);
        }else{
            System.out.println("datos incorrectos");
        }
        subtotal = (pagoBase * horasTrabNormal + horasTrabExtra) * (pagoBase * 2);
        return subtotal;
    }

    public float calcularImpuesto(int puesto, float horasTrabNormal, float horasTrabExtra){
        float subtotal = calcularSubtotal(puesto, horasTrabNormal, horasTrabExtra);
        float impuesto = (float) (subtotal * 0.16);
        return impuesto;
    }

    public float calcularTotal(int puesto, float horasTrabNormal, float horasTrabExtra){
        float subtotal = calcularSubtotal(puesto, horasTrabNormal, horasTrabExtra);
        float impuesto = calcularImpuesto(puesto, horasTrabNormal, horasTrabExtra);
        float total = subtotal - impuesto;
        return total;

    }



}

